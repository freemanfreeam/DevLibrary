For build and run project usage

docker-compose up -d --build

And then create db

open docker cli 

in terminal input commands

flask db init
flask db migrate
flask db upgrade

Then open browser with address http://localhost:8000

Stop project 

docker-compose down -v
