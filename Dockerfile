FROM python:3.10-alpine
WORKDIR /code
ENV FLASK_APP=run.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apk add --no-cache gcc musl-dev linux-headers libffi-dev
RUN apk add --update --no-cache python3 g++ make
RUN /bin/sh -c pip install -r requirements.txt
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
# EXPOSE 5000
COPY . .
# USER app
# ENTRYPOINT ["code/entry.sh"]