import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_ckeditor import CKEditor
from flask_mail import Mail
from config import Config

app = Flask(__name__, template_folder='templates')
app.config.from_object(Config)
app.config.from_pyfile('../config-extended.py')

db = SQLAlchemy(app)
migrate = Migrate(app, db)
ckeditor = CKEditor(app)
mail = Mail(app)

login_manager = LoginManager(app)
login_manager.login_view = 'admin_blueprint.login'

from app.admin.route import admin_bp
app.register_blueprint(admin_bp, url_prefix="/admin")

from app.main import bp as main_bp
app.register_blueprint(main_bp)

from app import models
