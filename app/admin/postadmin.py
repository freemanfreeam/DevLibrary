from flask_ckeditor import CKEditorField
from flask_admin.contrib.sqla import ModelView
from flask import redirect, abort, url_for, request
from flask_login import current_user


class PostAdmin(ModelView):
    form_overrides = dict(text=CKEditorField)
    create_template = 'post/edit.html'
    edit_template = 'post/edit.html'

    def is_accessible(self):
        return (current_user.is_active and
                current_user.is_authenticated and
                current_user.has_role('admin')
                )

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))

