from app.main import bp
from app.models import User, Role, Book, Tag, Level, Post
from flask import render_template, flash, redirect, url_for, request, current_app
from flask_login import current_user


@bp.route('/', methods=['GET', 'POST'])
def home():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.list_books', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.list_books', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('main/news.html', title=('posts'), posts=posts.items, 
        next_url=next_url, prev_url=prev_url, page=page)

@bp.route('/news', methods=['GET', 'POST'])
def news():
    return render_template('main/news.html', title=('news'))

@bp.route('/articles', methods=['GET', 'POST'])
def articles():
    return render_template('main/articles.html', title=('articles'))

@bp.route('/books', methods=['GET'])
def list_books():
    page = request.args.get('page', 1, type=int)
    books = Book.query.order_by(Book.year.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.list_books', page=books.next_num) \
        if books.has_next else None
    prev_url = url_for('main.list_books', page=books.prev_num) \
        if books.has_prev else None
    return render_template('main/books.html', title=('books'), books=books.items, 
        next_url=next_url, prev_url=prev_url, page=page)

@bp.route('/about', methods=['GET', 'POST'])
def about():
    return render_template('main/about.html', title=('about'))

@bp.route('/faq', methods=['GET', 'POST'])
def faq():
    return render_template('main/faq.html', title=('faq'))

@bp.route('/more/<int:id>', methods=['GET'])
def more(id):
    book = Book.query.filter_by(id=id).first_or_404()
    # if not current_user.is_authenticated:
    #     redirect('.admin.index')
    return render_template('main/more.html', title=('about book'), book=book)

@bp.route('/article/<int:id>', methods=['GET'])
def article(id):
    post = Post.query.filter_by(id=id).first_or_404()
    return render_template('main/article.html', post=post)


